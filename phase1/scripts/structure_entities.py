import sys, json

"""
    Filter goals:
        1. Receive the nodes that are tagged as amenities
        2. Remap the objects in order to rename keys and take only necessary node data
        3. Create a snapshot of the data
        4. Pass the data in json format to the next filter
"""

# 1. receive data from standard input
standard_input = sys.stdin.read()

# 2.1. convert data in python dictionary
nodes = json.loads(standard_input)

# 2.2. remap data
final_data = []
for node in nodes:
    mapped_node = dict()
    mapped_node['osm_id'] = int(node['@id'])
    mapped_node['lat'] = float(node['@lat'])
    mapped_node['lon'] = float(node['@lon'])
    mapped_tags = []
    for tag in node['tag']:
        mapped_tag = dict()
        mapped_tag['key'] = tag['@k']
        mapped_tag['value'] = tag['@v']
        mapped_tags.append(mapped_tag)

    mapped_node['tag'] = mapped_tags
    final_data.append(mapped_node)

# 3.1. return data to json format
data_json = json.dumps(final_data)

# 3.2 create snapshot of the data
with open('data/map_data.snapshot.6.json', 'w') as f: 
    f.write(data_json)

# 4. pass data in json format in next filter
sys.stdout.write(data_json)
