import sys, json

"""
    Filter goals:
        1. Receive the API response in json format from previous filter
        2. Extract only node data, stripping away unnecessary metadata, user data, 
            relations and ways (for more info refer: https://wiki.openstreetmap.org/wiki/Elements)
        3. Create a snapshot of the data
        4. Pass the data in json format to the next filter
"""

# 1. receive data from standard input
standard_input = sys.stdin.read()

# 2.1. convert data in python dictionary
data_dict = json.loads(standard_input)

# 2.2. strip away unnecessary data
nodes = data_dict['osm']['node']

# 3.1. return data to json format
data_json = json.dumps(nodes)

# 3.2 create snapshot of the data
with open('data/map_data.snapshot.3.json', 'w') as f: 
    f.write(data_json)

# 4. pass data in json format in next filter
print(data_json)


