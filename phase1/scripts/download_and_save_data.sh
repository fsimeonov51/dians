#!/bin/bash
FILE=data/map_data.snapshot.1.xml

if [ ! -d "./data/" ]; then
    mkdir ./data/
fi

if [ ! -f "$FILE" ]; then # comment out in development
    curl -s -o './data/map_data.snapshot.1.xml' https://overpass-api.de/api/map?bbox=21.2918,41.9357,21.5614,42.0535
# https://overpass-api.de/api/map?bbox=21.2918,41.9357,21.5614,42.0535 #Skopje
# https://overpass-api.de/api/map?bbox=21.41677,41.99957,21.42519,42.00325 # Debar Maalo
fi # comment out in development

cat data/map_data.snapshot.1.xml
