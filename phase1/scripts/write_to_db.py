import sys, json, os, urllib
from dotenv import load_dotenv
from pymongo import MongoClient

"""
    Filter goals:
        1. Receive the nodes that are tagged as amenities
        2. Write the data in nonrelational database
"""

# load environmental variables in order to get mongo connection string
load_dotenv()

# 1. receive data from standard input
standard_input = sys.stdin.read()

# 2. map environmental variables
username = urllib.parse.quote_plus(os.getenv('MONGODB_USERNAME'))
password = urllib.parse.quote_plus(os.getenv('MONGODB_PASSWORD'))
hostname = urllib.parse.quote_plus(os.getenv('MONGODB_HOSTNAME'))
database = urllib.parse.quote_plus(os.getenv('MONGODB_DATABASE'))


# 3. create database connection uri
db_uri = 'mongodb://' + username + ':' + password + '@' + hostname + ':27017/' + database

# 4. create PyMongo client
client = MongoClient(db_uri)
db = client[database]

# 5. list available collections (debugging)
collist = db.list_collection_names()
print(f'Collections: {collist}')

# 6. select required collection and add or update nodes
node_collection = db['nodes']
items = json.loads(standard_input)
for item in items:
    node_collection.replace_one({'osm_id': item['osm_id']}, item, upsert=True)

# 7. print out number of elements in database and close session
print(f'TOTAL NODES IN DB: {node_collection.count_documents({})}')
client.close()


