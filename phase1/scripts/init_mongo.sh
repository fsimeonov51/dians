mongo -- "$MONGO_INITDB_DATABASE" <<EOF
    var rootUser = '$MONGO_INITDB_ROOT_USERNAME';
    var rootPassword = '$MONGO_INITDB_ROOT_PASSWORD';
    var admin = db.getSiblingDB('admin');
    admin.auth(rootUser, rootPassword);

    var user = '$MONGODB_USERNAME';
    var passwd = '$MONGODB_PASSWORD';
    db.createUser({user: user, pwd: passwd, roles: [ "readWrite" ]});
    db.createCollection("roles")
    db.roles.insertMany([
       { name: "ROLE_USER" },
       { name: "ROLE_MODERATOR" },
       { name: "ROLE_ADMIN" },
    ])
EOF
