#!/bin/bash

scripts/download_and_save_data.sh |
    python scripts/parse.py |
    python scripts/extract_node_data.py |
    python scripts/extract_tagged_nodes.py |
    python scripts/extract_amenity_nodes.py |
    python scripts/structure_entities.py |
    python scripts/write_to_db.py
