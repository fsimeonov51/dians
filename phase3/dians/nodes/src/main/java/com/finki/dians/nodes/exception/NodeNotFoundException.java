package com.finki.dians.nodes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NodeNotFoundException extends RuntimeException {
    public NodeNotFoundException(String nodeId) {
        super(String.format("Node with id: %s not found", nodeId));
    }
}
