package com.finki.dians.nodes.services;

import com.finki.dians.nodes.model.Node;
import com.finki.dians.nodes.model.NodeFilter;
import com.finki.dians.nodes.model.RangeFilter;
import org.springframework.data.domain.Slice;

import java.util.List;

public interface NodeService {
    Slice<Node> getAll(RangeFilter rangeFilter, NodeFilter nodeFilter, Integer pageNumber, Integer pageSize);

    List<Node> getAll(RangeFilter rangeFilter, NodeFilter nodeFilter);

    Node getNode(String id);

    Node updateNode(String id, Node node);

    Node createNode(Node node);

    void deleteNode(String id);
}
