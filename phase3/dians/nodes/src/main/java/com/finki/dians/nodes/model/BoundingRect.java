package com.finki.dians.nodes.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BoundingRect {
    private Double minLat;
    private Double maxLat;
    private Double minLon;
    private Double maxLon;
}
