package com.finki.dians.nodes.controller;

import com.finki.dians.nodes.model.Node;
import com.finki.dians.nodes.model.NodeFilter;
import com.finki.dians.nodes.model.RangeFilter;
import com.finki.dians.nodes.services.NodeService;
import org.springframework.data.domain.Slice;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class NodeController {

    private final NodeService nodeService;

    public NodeController(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @GetMapping()
    public ResponseEntity<List<Node>> getAll(RangeFilter rangeFilter, NodeFilter nodeFilter) {
        List<Node> responseData = nodeService.getAll(rangeFilter, nodeFilter);
        return ResponseEntity.ok(responseData);
    }

    @GetMapping("/paged")
    public ResponseEntity<Slice<Node>> getAllPaged(RangeFilter rangeFilter,
                                                   NodeFilter nodeFilter,
                                                   @RequestParam(required = false, defaultValue = "0") Integer pageNumber,
                                                   @RequestParam(required = false, defaultValue = "10") Integer pageSize) {
        Slice<Node> responseData = nodeService.getAll(rangeFilter, nodeFilter, pageNumber, pageSize);
        return ResponseEntity.ok(responseData);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Node> getNode(@PathVariable String id) {
        Node responseData = nodeService.getNode(id);
        return ResponseEntity.ok(responseData);
    }

    @PostMapping
    public ResponseEntity<Node> createNode(@RequestBody Node node) {
        Node responseData = nodeService.createNode(node);
        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(responseData.getId())
                .toUri();
        return ResponseEntity
                .created(location)
                .body(responseData);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Node> updateNode(@PathVariable String id,
                                           @RequestBody Node node) {
        Node responseData = nodeService.updateNode(id, node);
        return ResponseEntity.ok(responseData);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteNode(@PathVariable String id) {
        nodeService.deleteNode(id);
    }
}
