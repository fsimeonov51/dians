package com.finki.dians.nodes.repository.impl;

import com.finki.dians.nodes.model.Node;
import com.finki.dians.nodes.repository.CustomNodeRepository;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomNodeRepositoryImpl implements CustomNodeRepository {

    private final MongoTemplate mongoTemplate;

    public CustomNodeRepositoryImpl(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    public List<Node> customQueryAll(Query q) {
        return mongoTemplate.find(q, Node.class);
    }

    @Override
    public Slice<Node> customQueryAll(Query q, Pageable pageable) {
        final Query request = q.with(pageable);
        List<Node> nodeList = mongoTemplate.find(request, Node.class);
        return PageableExecutionUtils.getPage(
                nodeList,
                pageable,
                () -> mongoTemplate.count(Query.of(request).limit(-1).skip(-1), Node.class));
    }
}
