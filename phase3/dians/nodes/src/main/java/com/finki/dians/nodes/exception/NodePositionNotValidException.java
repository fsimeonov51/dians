package com.finki.dians.nodes.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NodePositionNotValidException extends RuntimeException {
    public NodePositionNotValidException(String message) {
        super(message);
    }
}
