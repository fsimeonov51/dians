package com.finki.dians.nodes.repository;

import com.finki.dians.nodes.model.Node;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public interface CustomNodeRepository {
    List<Node> customQueryAll(Query q);

    Slice<Node> customQueryAll(Query q, Pageable pageable);
}
