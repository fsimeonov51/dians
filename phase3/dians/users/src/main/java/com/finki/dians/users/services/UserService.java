package com.finki.dians.users.services;

import com.finki.dians.users.model.User;

import java.util.List;

public interface UserService {

    List<User> getUsers();

    User getUserById(String userId);

    User createUser(User newUser);

    User updateUser(User user);

    void deleteUserById(String userId);
}
