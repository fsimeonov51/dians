package com.finki.dians.users.controller;

import com.finki.dians.users.model.User;
import com.finki.dians.users.payload.resources.UserResource;
import com.finki.dians.users.payload.resources.UserResourceAssembler;
import com.finki.dians.users.services.UserService;
import com.finki.dians.users.utils.Utils;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@AllArgsConstructor
@CrossOrigin(origins = "*")
@RestController
@PreAuthorize("hasRole('ADMIN')")
@RequestMapping(value = "/users")
public class UserController {

    private final UserService userService;
    private final UserResourceAssembler userResourceAssembler;

    @GetMapping(value = "/list")
    public ResponseEntity<?> getUsers() {
        return new ResponseEntity<>(userService.getUsers(), HttpStatus.OK);
    }


    @GetMapping(value = "/{userId}")
    public ResponseEntity<?> getUser(@PathVariable("userId") String userId) {
        return new ResponseEntity<>(userService.getUserById(userId), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> createUser(@RequestBody @Validated UserResource userResource, BindingResult result) {
        if (result.hasErrors()) {
            return new ResponseEntity<>(Utils.getDefaultMessageFromErrors(result.getAllErrors()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        User user = userResourceAssembler.fromResource(userResource);
        return new ResponseEntity<>(userService.createUser(user), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<?> updateUser(@RequestBody @Validated UserResource userResource, BindingResult result) {
        if (result.hasErrors()) {
            return new ResponseEntity<>(Utils.getDefaultMessageFromErrors(result.getAllErrors()), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        User user = userService.getUserById(userResource.getId());
        user = userResourceAssembler.fromResource(user, userResource);

        return new ResponseEntity<>(userService.updateUser(user), HttpStatus.OK);
    }

    @DeleteMapping(value = "/{userId}")
    public ResponseEntity<?> deleteUser(@PathVariable("userId") String userId) {
        userService.deleteUserById(userId);
        return new ResponseEntity<>(String.format("User [%s] is deleted!", userId), HttpStatus.OK);
    }

}
