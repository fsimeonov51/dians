package com.finki.dians.users.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.validation.ObjectError;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Utils {
    private static final String SPECIAL_CHARS_CLEARER = "[^a-zA-Z0-9/!_\\*'\\(\\)\\.-]*";

    public static <E extends Enum<E>> boolean isEnumValue(Class<E> enumData, String value) {
        for (Enum<E> enumVal : enumData.getEnumConstants()) {
            if (enumVal.toString().equals(value)) return true;
        }
        return false;
    }

    public static List<String> getDefaultMessageFromErrors(List<ObjectError> errors) {
        List<String> errorMessages = new ArrayList<>();
        errors.forEach(n -> errorMessages.add(n.getDefaultMessage()));
        return errorMessages;
    }

    /**
     * Method to clear all the special characters except the safe ones for AWS S3: !/.-_'*()
     * @param fileName
     * @return
     */
    public static String clearUnsafeCharactersFromFileName(String fileName){
        return fileName.replaceAll(SPECIAL_CHARS_CLEARER, "");
    }
}
