package com.finki.dians.users.repository;

import com.finki.dians.users.model.ERole;
import com.finki.dians.users.model.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface RoleRepository extends MongoRepository<Role, String> {

    Optional<Role> findByName(ERole name);

    boolean existsByName(String name);
}
