package com.finki.dians.users.model;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN
}
