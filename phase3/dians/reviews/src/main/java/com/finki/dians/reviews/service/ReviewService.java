package com.finki.dians.reviews.service;

import com.finki.dians.reviews.model.Review;
import org.springframework.data.domain.Page;

public interface ReviewService {
    Page<Review> getAllForNode(String amenityId, Integer pageNumber, Integer pageSize);

    void deleteReview(String id);

    Review addReview(Review review);
}
