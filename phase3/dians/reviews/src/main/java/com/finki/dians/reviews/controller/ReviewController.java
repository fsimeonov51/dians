package com.finki.dians.reviews.controller;

import com.finki.dians.reviews.model.Review;
import com.finki.dians.reviews.service.ReviewService;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
public class ReviewController {

    private final ReviewService reviewService;

    public ReviewController(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    @GetMapping("/{nodeId}")
    public ResponseEntity<Page<Review>> getReviewsForNode(@PathVariable String nodeId,
                                                          @RequestParam(required = false, defaultValue = "0") Integer pageNumber,
                                                          @RequestParam(required = false, defaultValue = "10") Integer pageSize) {
        Page<Review> responseData = reviewService.getAllForNode(nodeId, pageNumber, pageSize);
        return ResponseEntity.ok(responseData);
    }

    @PostMapping("/create")
    public ResponseEntity<Review> addReview(@RequestBody Review review) {
        Review responseData = reviewService.addReview(review);
        return ResponseEntity.ok(responseData);
    }

    @DeleteMapping("/{reviewId}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteReview(@PathVariable String reviewId) {
        reviewService.deleteReview(reviewId);
    }
}
