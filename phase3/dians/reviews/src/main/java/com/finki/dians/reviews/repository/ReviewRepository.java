package com.finki.dians.reviews.repository;

import com.finki.dians.reviews.model.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface ReviewRepository extends MongoRepository<Review, String> {

    Page<Review> findAllByNodeId(String nodeId, Pageable pageable);

    @Override
    Review save(Review s);

    @Override
    Optional<Review> findById(String s);

    @Override
    void deleteById(String s);
}
