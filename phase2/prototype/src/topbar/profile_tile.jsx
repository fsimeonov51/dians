import React from "react";
import {ClickAwayListener, Grow, makeStyles, MenuItem, MenuList, Paper, Popper, Typography} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {AuthActions} from "../store/actions/authActions";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    media: {
        borderRadius: "50%",
        height: "6vh",
        width: "6vh",
        padding: theme.spacing(1, 2)
    },
    menu: {
        marginTop: theme.spacing(8)
    }
}))


export const ProfileTile = () => {
    const classes = useStyles()
    const currentUser = useSelector(state => state.auth.currentUser);
    const dispatch = useDispatch();
    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }
        setOpen(false);
    };

    const handleLogout = () => {
        dispatch(AuthActions.signOut())
    }

    const handleListKeyDown = (event) => {
        if (event.key === 'Tab') {
            event.preventDefault();
            setOpen(false);
        }
    }

    const prevOpen = React.useRef(open);
    React.useEffect(() => {
        if (prevOpen.current === true && open === false) {
            anchorRef.current.focus();
        }

        prevOpen.current = open;
    }, [open]);

    return (
        <div className={classes.root}>
            <img src={"https://static01.nyt.com/images/2017/05/27/arts/27SHAKIRA/27SHAKIRA-superJumbo.jpg"}
                 className={classes.media}
                 alt={""}
            />
            <Typography variant={"h6"} onClick={handleToggle} ref={anchorRef}>
                {currentUser.username}
            </Typography>

            <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                {({TransitionProps, placement}) => (
                    <Grow
                        {...TransitionProps}
                        style={{transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom'}}
                    >
                        <Paper>
                            <ClickAwayListener onClickAway={handleClose}>
                                <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                                    <MenuItem onClick={handleLogout}>Logout</MenuItem>
                                </MenuList>
                            </ClickAwayListener>
                        </Paper>
                    </Grow>
                )}
            </Popper>
        </div>
    )
}