import React from 'react'
import logo from '../assets/logo.png'
import {AppBar, Button, makeStyles, Toolbar, Typography} from "@material-ui/core";
import {Link, useHistory} from "react-router-dom";
import {ProfileTile} from "./profile_tile";
import {useSelector} from "react-redux";

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    title: {
        display: 'flex',
        alignItems: 'center',
        textDecoration: "none",
        color: "white",
    },
    media: {
        height: "6vh",
        padding: theme.spacing(2, 0)
    },
    login_btn: {
        marginLeft: theme.spacing(2)
    },
    stickRight: {
        marginLeft: "auto",
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        width: '50%'
    }
}));

export const TopBar = () => {
    const classes = useStyles();
    const history = useHistory();
    let currentUser = useSelector(state => state.auth.currentUser)

    const handleLogin = () => {
        history.push('/auth/login')
    }

    const handleCreateAmenity = () => {
        history.push('/amenity/create')
    }

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Link to={"/"} className={classes.title} replace={false}>
                        <img src={logo} className={classes.media} alt={"App Logo"}/>
                        <Typography variant="h5">
                            FindABar.com
                        </Typography>
                    </Link>
                    <div className={classes.stickRight}>
                        {currentUser && <Button color="default" variant={"contained"} onClick={handleCreateAmenity}>
                            New amenity +
                        </Button>}
                        {
                            !currentUser ? <Button color="secondary" variant={"contained"} onClick={handleLogin}
                                                   className={classes.login_btn}>
                                Login
                            </Button> : <ProfileTile/>
                        }
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    )
}
