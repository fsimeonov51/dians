import {Route, Switch} from 'react-router-dom'
import {LandingPage} from "../pages/landing";
import {AmenityDetailsPage} from "../pages/amenity_details";
import {AmenityCreatePage} from "../pages/create_amenity";
import {LoginPage} from "../pages/login";
import {RegisterPage} from "../pages/register";
import React, {useEffect} from "react";
import {Layout} from "../layout/layout";
import {useDispatch} from "react-redux";
import {AUTH_TOKEN_KEY} from "../axios/axios";
import {AuthActions} from "../store/actions/authActions";

export const App = () => {

    const dispatch = useDispatch();
    const existingToken = localStorage.getItem(AUTH_TOKEN_KEY);

    useEffect(() => {
        dispatch(AuthActions.updateToken(existingToken));
    }, [])

    return (
        <Layout>

            <Switch>
                <Route path={'/amenity/create'} component={AmenityCreatePage}/>
                <Route path={'/amenity/:amenity_id'} component={AmenityDetailsPage}/>
                <Route path={'/auth/login'} component={LoginPage}/>
                <Route path={'/auth/register'} component={RegisterPage}/>
                <Route path={'/'} component={LandingPage}/>
            </Switch>
        </Layout>
    );
}

export default App;
