import React, {useState} from 'react'
import {Button, makeStyles, TextField, Typography} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {useHistory} from 'react-router-dom'
import {useDispatch} from "react-redux";
import {AuthActions} from "../store/actions/authActions";

const useStyles = makeStyles((theme) => ({
    root: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        height: "80vh"
    },
    form: {
        padding: theme.spacing(4),
        borderRadius: "15px",
        width: "25%",
    },
    buttons: {
        display: "flex",
        alignItems: "center",
        justifyContent: "space-evenly",
        marginTop: theme.spacing(4)
    },
    btn: {
        width: "35%"
    }
}));

export const LoginPage = () => {
    const classes = useStyles();
    const history = useHistory();
    const dispatch = useDispatch();
    const [formData, setFormData] = useState({
        username: "",
        password: ""
    })

    const handleChange = (e) => {
        const {name, value} = e.target
        setFormData({
            ...formData,
            [name]: value
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        dispatch(AuthActions.signIn(formData.username, formData.password))
        history.push('/')
    }

    const handleRegister = () => {
        history.push('/auth/register')
    }

    return (
        <div className={classes.root}>
            <Grid container className={classes.form} component={"form"} noValidate autoComplete="off" spacing={3}>
                <Grid item sm={12}>
                    <Typography variant={'h6'}>
                        Login form
                    </Typography>
                </Grid>
                <Grid item xs={12}>
                    <TextField id="username"
                               name={"username"}
                               type={"text"}
                               label="Username"
                               variant="outlined"
                               fullWidth
                               value={formData.username}
                               onChange={handleChange}
                    />
                </Grid>
                <Grid item xs={12}>
                    <TextField id="password"
                               name={"password"}
                               label="Password"
                               variant="outlined"
                               type={"password"}
                               fullWidth
                               value={formData.password}
                               onChange={handleChange}
                    />
                </Grid>
                <Grid item xs={12} className={classes.buttons}>
                    <Button variant={"contained"}
                            color={"primary"}
                            className={classes.btn}
                            onClick={handleRegister}>
                        Register
                    </Button>
                    <Button variant={"contained"}
                            color={"secondary"}
                            className={classes.btn}
                            type={"submit"}
                            onClick={handleSubmit}
                    >
                        Login
                    </Button>
                </Grid>
            </Grid>
        </div>
    )
}
