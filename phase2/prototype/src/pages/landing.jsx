import React, {useRef, useState} from 'react'
import Map from "../amenities/amenity_map";
import {AmenityList} from "../amenities/amenity_list";
import {Grid, makeStyles} from "@material-ui/core";
import {AmenityFilter} from "../amenities/amenity_filter/amenity_filter";
import {AmenityShortDetails} from "../amenities/amenity_short_details";
import {useSelector} from "react-redux";

const useStyles = makeStyles((theme) => ({
    root: {
        maxHeight: "91vh",
        minHeight: "91vh",
        overflowY: "hidden"
    },
    amenityList: {
        backgroundColor: theme.palette.background.default,
        overflowY: 'scroll',
        height: '100%'
    },
    map: {
        position: 'relative',
        overflowX: "hidden"
    },
    amenityShortDetails: {
        zIndex: 100
    }
//    style={{height: window.innerHeight - containerRef.current?.offsetTop + 'px'}}
}))

export const LandingPage = () => {

    const classes = useStyles()

    const amenities = useSelector(state => state.amenities.amenities);
    const containerRef = useRef(null);
    const heightClass = {
        maxHeight: window.innerHeight - (containerRef.current ? containerRef.current.offsetTop : 0) + "px"
    }
    const [shortItem, setShortItem] = useState(null)
    const setShortItemHandler = (id) => {
        let amenity = amenities.find(amenity => amenity.id === id);
        setShortItem(amenity);
    }

    const removeShortItem = () => {
        setShortItem(null);
    }

    return (
        <React.Fragment>
            <AmenityFilter/>
            <Grid container className={classes.root} ref={containerRef}
                  style={heightClass}>
                <Grid item md={3} className={classes.amenityList}
                      style={heightClass}>
                    <AmenityList amenities={amenities} onItemClick={setShortItemHandler}/>
                </Grid>
                <Grid item md={9} className={classes.map} style={heightClass}>
                    {shortItem && <AmenityShortDetails item={shortItem}
                                                        onCancelClick={removeShortItem}
                                                        className={classes.amenityShortDetails}/>}
                    <Map lat={41.9981} lng={21.4254} zoom={18} onItemClick={setShortItemHandler}
                         amenities={amenities}/>
                </Grid>
            </Grid>
        </React.Fragment>
    )
}
