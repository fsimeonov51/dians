import React, {useRef, useState} from "react";
import {AmenityForm} from "../amenities/amenity_form";
import Map from "../amenities/amenity_map";
import {Grid, makeStyles} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";


const useStyles = makeStyles((theme) => ({
    root: {
        maxHeight: "91vh",
        minHeight: "91vh",
        overflowY: "hidden"
    },
    amenityList: {
        backgroundColor: theme.palette.background.default,
        overflowY: 'scroll',
        height: '100%'
    },
    map: {
        position: 'relative'
    },
//    style={{height: window.innerHeight - containerRef.current?.offsetTop + 'px'}}
}))

export const AmenityCreatePage = () => {
    const classes = useStyles()
    const dispatch = useDispatch();
    const amenities = useSelector(state => state.amenities.amenities);
    const center = useSelector(state => state.amenities.mapCenter)
    const containerRef = useRef(null);

    const [is_pin_clicked, set_is_pin_clicked] = useState(false);


    const setPinToMap = () => {
        set_is_pin_clicked(true);
        dispatch({
            type: "ADD_PIN", pinCoordinates: center
        })
    }

    const heightClass = {
        maxHeight: window.innerHeight - (containerRef.current ? containerRef.current.offsetTop : 0) + "px"
    }

    return (
        <Grid container className={classes.root} ref={containerRef}
              style={heightClass}>
            <Grid item lg={3} style={{padding: '2rem'}}>
                <AmenityForm onPinToMapClick={setPinToMap}/>
            </Grid>
            <Grid item lg={9}>
                <Map lat={52.7} lng={52.5} zoom={18} isSetPinClicked={is_pin_clicked}
                     amenities={amenities} className={classes.root}/>
            </Grid>
        </Grid>
    )
}
