import React, {useState} from "react";
import {makeStyles} from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import {AmenityWorkHoursPicker} from "./amenity_filter/amenity_workhours_picker";
import {AmenityCategoryPicker} from "./amenity_filter/amenity_category_picker";
import {Typography} from "@material-ui/core";
import {convertToIso} from "../utils/utils";
import {useDispatch, useSelector} from "react-redux";
import {AmenityActions} from "../store/actions/amenityActions";

const useStyles = makeStyles(() => ({
    center: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    full_width: {
        width: '100%',
    }
}));


export const AmenityForm = (props) => {

    const classes = useStyles();
    const pinCoordinates = useSelector(state => state.amenities.pinCoordinates)
    const dispatch = useDispatch();
    const initCat = ["coffee", "restaurant", "club", "bar", "pub"]
        .map(it => {
            return {
                key: it,
                value: false
            }
        })
    const [categories, setCategories] = useState({categories: initCat})
    const [state, setState] = useState({
        coffee: true,
        restaurant: true,
        club: true,
        bar: false,
        something: false
    })

    const [formData, setFormData] = useState({
        name: "",
        address: "",
        phone_number: "",
        description: "",
        from: 1608249634486,
        to: 1608289203872,
    })

    const handleStateChange = (e) => {
        const {name, checked} = e.target
        setState({
            ...state,
            [name]: checked
        })

        const newCat = {
            key: name,
            value: checked
        }
        let c = categories.categories;
        c.find(v => v.key === newCat.key).value = checked;
        setCategories({
            categories: c
        })
    }

    const handleChange = (e) => {
        const {name, value} = e.target
        setFormData({
            ...formData,
            [name]: value
        })
    }

    const handleHoursChange = (name, date_str) => {
        setFormData({
            ...formData,
            [name]: date_str
        })
    }
    const convertToTag = (key, value) => {
        return {
            key,
            value
        }
    }

    const createPostBody = (tags, amenities, coordinates) => {
        let tagsList = [];
        for (const [key, value] of Object.entries(tags))
            tagsList.push(convertToTag(key, value))

        for (const [key, value] of Object.entries(amenities)) {
            if (value) {
                tagsList.push(convertToTag('amenity', key))
            }
        }
        return {
            ...coordinates,
            tags: tagsList
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        let postBody = createPostBody({
            name: formData.name,
            address: formData.address,
            phone_number: formData.phone_number,
            description: formData.description,
            opening_hours: `${convertToIso(formData.from)}-${convertToIso(formData.to)}`
        }, state, {
            latitude: pinCoordinates.lat,
            longitude: pinCoordinates.lng
        });

        dispatch(AmenityActions.postAmenity(postBody));

    }


    return (
        <Grid container component={'form'} className={classes.root} noValidate autoComplete="off" spacing={3}>
            <Grid item sm={12}>
                <Typography variant={'h6'}>
                    Create amenity form
                </Typography>
            </Grid>
            <Grid item sm={12}>
                <TextField id="name"
                           name={"name"}
                           type={"text"}
                           label="Name"
                           variant="outlined"
                           fullWidth
                           value={formData.name}
                           onChange={handleChange}
                />
            </Grid>
            <Grid item lg={12}>
                <TextField id="address"
                           name={"address"}
                           type={"text"}
                           label="Address"
                           variant="outlined"
                           fullWidth
                           value={formData.address}
                           onChange={handleChange}
                />
            </Grid>
            <Grid item lg={12}>
                <TextField id="phone_number"
                           name={"phone_number"}
                           type={"text"}
                           label="Phone number"
                           variant="outlined"
                           fullWidth
                           value={formData.phone_number}
                           onChange={handleChange}
                />
            </Grid>
            <Grid item lg={12}>
                <TextField id="description"
                           name={"description"}
                           type={"text"}
                           label="Description"
                           variant="outlined"
                           fullWidth
                           value={formData.description}
                           onChange={handleChange}
                />
            </Grid>
            <br/>
            <Grid item sm={12}>
                <AmenityWorkHoursPicker className={classes.full_width}
                                        handleChange={handleHoursChange}
                                        hours={formData}
                />
            </Grid>

            <Grid item sm={12}>
                <AmenityCategoryPicker onChange={handleStateChange} categories={categories.categories}/>
            </Grid>

            <Grid item sm={12} className={classes.center}>
                <Button variant="contained" size={'small'} onClick={props.onPinToMapClick}>
                    Pin to map
                </Button>
            </Grid>
            <Grid item sm={12} className={classes.center}>
                <Button variant="contained" color="secondary" size={'large'} onClick={handleSubmit}>
                    Submit
                </Button>
            </Grid>
        </Grid>
    )
}
