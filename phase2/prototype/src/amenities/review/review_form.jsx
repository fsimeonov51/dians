import React, {useState} from "react";
import {Button, makeStyles, TextField, Typography} from "@material-ui/core";
import ReactStars from "react-stars";
import {useDispatch, useSelector} from "react-redux";
import {ReviewActions} from "../../store/actions/reviewActions";

const useStyles = makeStyles(theme => ({
    root: {
        background: theme.palette.background.default,
        padding: theme.spacing(3),
        borderRadius: theme.spacing(3),
    },
    bolded: {
        fontWeight: 'bold',
    },
    padded: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(1)
    },
    right: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        paddingTop: theme.spacing(3)
    },
    rating: {
        display: 'flex',
        width: "15%",
        justifyContent: 'flex-start',
        alignItems: 'center'
    }
}))

export const ReviewForm = ({className, nodeId}) => {
    const classes = useStyles()
    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.auth.currentUser)
    const defaultContent = {
        content: "",
        rating: 0
    }

    const [state, setStateHandler] = useState(defaultContent)

    const handleChange = (e) => {
        setStateHandler({
            content: e.target.value,
            rating: state.rating
        })
    }

    const handleStars = (m) => {
        setStateHandler({
            content: state.content,
            rating: m
        })
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        if (!currentUser) {
            alert("You have to be signed up to post a comment")
            return;
        }
        dispatch(ReviewActions.postReview({
            ...state,
            nodeId,
            username: currentUser.username
        }))
        setStateHandler(defaultContent)
    }

    return (
        <form className={[className, classes.root].join(' ')}>
            <Typography variant={'button'} className={classes.bolded}>
                Share a review...
            </Typography>
            <div className={[classes.rating, classes.padded].join(' ')}>
                <Typography variant={'caption'}>
                    Rating:
                </Typography>
                <ReactStars
                    count={5}
                    value={state.rating}
                    size={24}
                    edit={true}
                    color2={'#ffd700'}
                    onChange={handleStars}
                />
            </div>
            <TextField label="Review Content"
                       type="text"
                       fullWidth
                       variant="outlined"
                       multiline
                       rows={5}
                       disabled={!currentUser}
                       onChange={handleChange}
            />

            <div style={{textAlign: "right"}}>
                <Button type={'submit'} onClick={handleSubmit} variant={'contained'} color={'primary'}
                        className={classes.padded}>
                    Post
                </Button>
            </div>
        </form>
    )
}
