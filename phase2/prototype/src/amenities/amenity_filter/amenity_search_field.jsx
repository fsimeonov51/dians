import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import search_svg from '../../assets/magnifying-glass.svg'
import {InputAdornment, TextField} from "@material-ui/core";

const useStyles = makeStyles(() => ({
    img: {
        height: 15
    },
}));

export const SearchField = ({className, value, onChange}) => {
    const classes = useStyles();

    const handleSearch = (e) => {
        e.preventDefault();
    }
    return (
        <form noValidate onSubmit={handleSearch} className={className}>
            <TextField
                variant={"outlined"}
                size={"small"}
                fullWidth
                value={value}
                label={"Search Amenity by Name"}
                InputProps={{
                    endAdornment: <InputAdornment position="end">
                        <IconButton type={"submit"}>
                            <img src={search_svg} alt="Search Icon" className={classes.img}/>
                        </IconButton>
                    </InputAdornment>,
                }}
                onChange={e => onChange(e.target.value)}
            />
        </form>
    );
}
