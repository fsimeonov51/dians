import React, {useState} from "react";
import {Button, Grid, makeStyles} from "@material-ui/core";
import {SearchField} from "./amenity_search_field";
import {AmenityCategoryPicker} from "./amenity_category_picker";
import {AmenityWorkHoursPicker} from "./amenity_workhours_picker";
import {AmenityActions} from "../../store/actions/amenityActions";
import {useDispatch, useSelector} from "react-redux";
import {convertToIso} from "../../utils/utils";

const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: theme.palette.background.default,
        padding: theme.spacing(3, 0, 2, 0),
        display: 'flex',
        alignItems: 'center',
    },
    search_field: {
        margin: theme.spacing(0, 1)
    },
    category_picker: {
        margin: theme.spacing(0, 2)
    },
    filter_group: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-evenly'
    },
    firstBtn: {
        marginRight: theme.spacing(3),
    }
}))

export const AmenityFilter = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const center = useSelector(state => state.amenities.mapCenter)
    const categories = ["coffee", "restaurant", "club", "bar", "pub"]
        .map(it => {
            return {
                key: it,
                value: false
            }
        })
    const initState = {
        categories,
        from: null,
        to: null,
        nameQuery: ""
    }
    const [state, setState] = useState(initState)


    const handleChange = (e) => {
        const {name, checked} = e.target
        const newCat = {
            key: name,
            value: checked
        }
        let categories = state.categories;
        categories.find(v => v.key === newCat.key).value = checked;
        setState({
            ...state,
            categories: categories
        })
    }

    const handleHoursChange = (name, date_str) => {
        setState({
            ...state,
            [name]: date_str
        })
    }

    const formatRequestThenDispatch = () => {
        let filter = {
            lat: center.lat,
            lng: center.lng,
            workHoursFrom: convertToIso(state.from),
            workHoursTo: convertToIso(state.to),
            typeQueries: state.categories.filter(it => it.value).map(it => it.key),
            nameQuery: state.nameQuery
        }
        dispatch(AmenityActions.fetchAllAmenitiesFiltered(filter))
    }

    return (
        <Grid container className={classes.root}>
            <Grid item xs={3}>
                <SearchField className={classes.search_field}
                             onChange={e => setState({...state, nameQuery: e})}
                             value={state.nameQuery}/>
            </Grid>
            <Grid item xs={6} className={classes.filter_group}>
                <AmenityCategoryPicker className={classes.category_picker}
                                       onChange={handleChange}
                                       categories={state.categories}
                />
                <AmenityWorkHoursPicker handleChange={(name, value) => handleHoursChange(name, value)}
                                        hours={{
                                            from: state.from,
                                            to: state.to
                                        }}/>
            </Grid>
            <Grid item xs={3}>
                <Button variant={"contained"} color={"secondary"} className={classes.firstBtn}
                        onClick={formatRequestThenDispatch}>
                    Apply Filter
                </Button>
                <Button variant={"contained"} color={"default"} onClick={() => {
                    setState(initState);
                    formatRequestThenDispatch()
                }}>
                    Clear Filter
                </Button>
            </Grid>
        </Grid>
    )


}
