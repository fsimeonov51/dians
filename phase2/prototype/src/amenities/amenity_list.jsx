import React from "react";
import {AmenityCard} from "./amenity_card";
import {makeStyles} from "@material-ui/core";


const useStyles = makeStyles((theme) => ({
    root: {
        padding: theme.spacing(0, 1)
    },
    card: {
        marginBottom: theme.spacing(2)
    }
}))

export const AmenityList = ({amenities, onItemClick}) => {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            {
                amenities && amenities.map((am, i) => <AmenityCard
                    key={i}
                    tags={am.tags}
                    onClick={() => onItemClick(am.id)}
                    style={{cursor: 'pointer'}}
                />)
            }
        </div>
    )
}