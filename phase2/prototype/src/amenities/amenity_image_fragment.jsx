import React from "react";
import Grid from "@material-ui/core/Grid";
import {Typography} from "@material-ui/core";
import ReactStars from "react-stars";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
    img: {
        borderRadius: "50%",
        width: '180px',
        height: '180px',
        objectFit: "cover",
    }
}))

export const AmenityImageFragment = ({img, title, street, stars, opening_hours}) => {
    const classes = useStyles()
    return (
        <Grid container>
            <Grid item xs={5}>
                <img src={img} alt="Amenity Image" className={classes.img}/>
            </Grid>
            <Grid item xs={7}>
                <Typography variant={'h5'}>
                    {title}
                </Typography>
                <Typography variant={'h6'} color="textSecondary">
                    {street}
                </Typography>
                {opening_hours ? opening_hours.split(new RegExp('[,;]'), 2).map(x => {
                    return (
                        <Typography variant="subtitle1" color="textSecondary"
                                    className={classes.hours}>
                            {x}
                        </Typography>
                    )
                }) : ''}
                <ReactStars
                    count={5}
                    value={stars}
                    size={24}
                    edit={false}
                    color2={'#ffd700'}/>
            </Grid>
        </Grid>
    )
}
