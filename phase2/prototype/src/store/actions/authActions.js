import axios from "../../axios/axios";
import {SIGN_IN, SIGN_OUT, UPDATE_TOKEN} from "./actionTypes";

export const AuthActions = {

    signUp: (username, email, password, roles) => dispatch => {
        axios.post('/auth/signup', {
            username,
            email,
            password,
            role: roles
        }).then(() => {
            dispatch(AuthActions.signIn(username, password))
        }).catch((resp) => {
            alert(resp.message)
        })
    },

    signIn: (username, password) => dispatch => {
        axios.post('/auth/signin', {
            username,
            password
        }).then((jwtResponse) => {
            const respData = jwtResponse.data;
            const token = respData.token;
            const user = {
                id: respData.id,
                username: respData.username,
                email: respData.email,
                roles: respData.roles
            }
            dispatch({
                type: SIGN_IN,
                payload: {
                    token,
                    user
                }
            })
        }).catch((resp) => {
            alert(resp.message)
        })
    },

    signOut: () => dispatch => {
        dispatch({
            type: SIGN_OUT,
        })
    },

    updateToken: (token) => dispatch => {
        dispatch({
            type: UPDATE_TOKEN,
            payload: token,
        })
    }

}
