import axios from "../../axios/axios";
import qs from "qs";
import {FETCHED_AMENITIES, FETCHED_AMENITY, SET_MAP_CENTER} from "./actionTypes";

export const AmenityActions = {

    fetchAllAmenities: center => dispatch => {
        const queryString = qs.stringify(center);
        axios.get(`/nodes?${queryString}`)
            .then(amenities => {
                dispatch({
                    type: FETCHED_AMENITIES,
                    amenities: amenities.data
                });
            })
            .catch(alert)
    },

    fetchAllAmenitiesFiltered: filter => dispatch => {
        const queryString = qs.stringify(filter);
        axios.get(`/nodes?${queryString}`)
            .then(amenities => {
                dispatch({
                    type: FETCHED_AMENITIES,
                    amenities: amenities.data
                });
            })
            .catch(alert)
    },

    fetchAmenity: amenity_id => dispatch => {
        axios.get(`/nodes/${amenity_id}`)
            .then((amenity) => {
                dispatch({type: FETCHED_AMENITY, amenity: amenity.data});
            })
    },

    postAmenity: postBody => dispatch => {
        axios.post('/nodes', postBody).then((amenity) => {
            dispatch({type: FETCHED_AMENITY, amenity: amenity.data});
            alert('Success')
        }).catch(() => {
            alert('That position is taken');
        })
    },

    updateMapCenter: (lat, lng) => dispatch => {
        dispatch({
            type: SET_MAP_CENTER,
            payload: {
                lat, lng
            }
        })
    }
}
