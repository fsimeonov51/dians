import axios from "../../axios/axios";
import {ADD_REVIEW, DELETE_REVIEW, FETCHED_REVIEWS} from "./actionTypes";

export const ReviewActions = {
    fetchAllReviews: nodeId => dispatch => {
        axios.get(`/reviews/${nodeId}`)
            .then(reviews => {
                dispatch({type: FETCHED_REVIEWS, reviews: reviews.data.content});
            })
    },

    postReview: review => dispatch => {
        axios.post('/reviews', {
            ...review,
            timestamp: new Date().getTime(),
            userId: review.username
        }).then((review) => {
            dispatch({
                type: ADD_REVIEW,
                review: review.data
            })
        })
    },

    deleteReview: reviewId => dispatch => {
        axios.delete(`/reviews/${reviewId}`).then(() => {
            dispatch({
                type: DELETE_REVIEW,
                reviewId
            })
        })

    }
}
