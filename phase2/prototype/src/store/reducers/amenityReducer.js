import {FETCHED_AMENITIES, FETCHED_AMENITY, SET_MAP_CENTER} from "../actions/actionTypes";

const initialState = {
    amenities: [],
    reviews: [],
    user: {},
    mapCenter: {lat: 41.9981, lng: 21.4254},
    pinCoordinates: {lat: null, lng: null}
}

export const amenityReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_MAP_CENTER:
            return {
                ...state,
                mapCenter: action.payload
            }
        case FETCHED_AMENITIES:
            return {
                ...state,
                amenities: action.amenities
            }
        case FETCHED_AMENITY:
            return {
                ...state,
                amenities: [...state.amenities, action.amenity]
            }
        case 'REMOVE_AMENITY':
            return {
                ...state,
                amenities: state.amenities.filter(x => x.id !== action.amenity.id)
            }
        case 'ADD_PIN':
            return {
                ...state,
                pinCoordinates: action.pinCoordinates
            }
        default:
            return state
    }
}
