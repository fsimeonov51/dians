import React from "react";
import {TopBar} from "../topbar/topbar";
import {makeStyles} from "@material-ui/core";
import {BrowserRouter} from "react-router-dom";
import DateFnsUtils from "@date-io/date-fns";
import {MuiPickersUtilsProvider} from "@material-ui/pickers";

const useStyles = makeStyles(() => ({
    root: {}
}))

export const Layout = (props) => {
    const classes = useStyles();
    return (
        <React.Fragment>
            <BrowserRouter>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <TopBar/>
                    <main className={classes.root}>
                        {props.children}
                    </main>
                </MuiPickersUtilsProvider>
            </BrowserRouter>
        </React.Fragment>
    )
}
